import java.util.Scanner;

public class NationalPark {
	public static void main (String[] args) {
		Scanner sc = new Scanner (System.in);
		
		Cat[] clowder = new Cat[4];
		for (int i = 0; i < 4; i++) {
			System.out.println("What is the cat's name?");
			String name = sc.next();
			System.out.println("What is it's color?");
			String color = sc.next();
			System.out.println("What about the age?");
			int age = sc.nextInt();
			
			clowder[i] = new Cat(name, color, age);
		}
		//Previous loop that didn't involve constructor
		/*Cat[] clowder = new Cat[4];
		for (int i = 0; i < 4; i++) {
			clowder[i] = new Cat();
			System.out.println("What is the cat's name?");
			clowder[i].setName(sc.next());
			System.out.println("What is it's color?");
			clowder[i].setColor(sc.next());
			System.out.println("What about the age?");
			clowder[i].setAge(sc.nextInt());
		}*/
		
		System.out.println("Info about the last cat in your clowder:");
		System.out.println("Name: " + clowder[3].getName() + ", Color: " + clowder[3].getColor() + ", Age: " + clowder[3].getAge());
		
		clowder[0].catPresent();
		clowder[0].catAgeToHumanAge();
		
		//Lab4
		System.out.println("Lab 4 additions: ");
		
		System.out.println("Time to feed your cat! Give it between 1 and 80 grams of food.");
		clowder[1].eatFood(sc.nextInt());
		
		//Lab4 part 3
		System.out.println("Inside of the final part!");
		System.out.println("Oops! Your 4th cat needs to be replaced!");
		System.out.println("Your " + clowder[3].getColor() + " cat, " + clowder[3].getName() + ", of age of " + clowder[3].getAge() + " is taken away for treatment.");
		System.out.println("What's your new cat's name?");
		clowder[3].setName(sc.next());
		System.out.println("What about the color?");
		clowder[3].setColor(sc.next());
		System.out.println("And the age?");
		clowder[3].setAge(sc.nextInt());
		System.out.println("Perfect! Your new cat, " + clowder[3].getName() + ", is " + clowder[3].getColor() + " and " + clowder[3].getAge() + " years old.");
		
	}
}