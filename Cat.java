public class Cat {
	private String name;
	private String color;
	private int age;
	
	public void catPresent() {
		System.out.println("Surprise! You got a new " + color + " cat named " + name);
	}
	
	public void catAgeToHumanAge() {
		System.out.println("Wondering whether your cat is still in his youth or maybe is going through his midlife crisis? This method will help!");
		printAge();
		System.out.println("In human years: " + (age * 7));
	}
	
	public void eatFood(int grams) {
		if (grams == 0) {
			System.out.println("You didn't give any food! The cat is still hungry");
		}
		else if (grams > 0 && grams <=20) {
			System.out.println("That's not enough food!");
			this.age = age + 1;
		}
		else if (grams > 20 && grams <=40) {
			System.out.println("That's a decent amount of food!");
			this.age = age + 2;
		}
		else if (grams > 40 && grams <=60) {
			System.out.println("That's a good amount of food!");
			this.age = age + 3;
		}
		else if (grams > 60 && grams <=80) {
			System.out.println("That's a lot of food!");
			this.age = age + 4;
		}
		else {
			System.out.println("That's too much food! Your cat overate and died :(");
			this.age = 0;
		}
		printAge();
	}
	
	//Constructor
	public Cat(String name, String color, int age) {
		this.name = name;
		this.color = color;
		this.age = age;
	}
	
	//Helper Method
	private void printAge() {
		System.out.println("Your cat, " +name +" is " +age +" years old now");
	}
	
	//Getters
	public String getName() {
		return this.name;
	}
	public String getColor() {
		return this.color;
	}
	public int getAge() {
		return this.age;
	}
	
	//Setters
	public void setName (String newName) {
		this.name = newName;
	}
	public void setColor (String newColor) {
		this.color = newColor;
	}
	public void setAge(int newAge) {
		this.age = newAge;
	}
}